# Proof of concept
from flask import Flask, g, request
from requests import request as r

app = Flask(__name__)
SITE_NAME = 'http://jsonplaceholder.typicode.com/'
short_sn = 'jsonplaceholder.typicode.com'
#SITE_NAME = 'http://postman-echo.com/'
#short_sn = 'postman-echo.com'

methods = ['GET','POST','DELETE']

def ret_helper(app, response, status, mimetype):
    response = app.response_class(
        response=response,
        status=status,
        mimetype=mimetype
    )
    return response



@app.route('/', defaults={'path': ''}, methods = methods)
@app.route('/<path:path>', methods = methods)
def proxy(path):

    # Check if request is permitted
    if 'albums' in path:

        # Here we prepare the headers to send to the target.
        # Set the host header to be target
        #print("Headers from client: " + str(dict(request.headers)))
        headers = dict(request.headers)
        headers['host'] = short_sn

        #print("About to poke remote server")
        # Make proxy request
        pr = r(request.method,f'{SITE_NAME}{path}', headers=headers)

        # return
        #print("Response from remote server: ")
        #print(pr.content)
        #print(pr.status_code)

        return ret_helper(app, pr.content, pr.status_code, pr.headers['content-type'])
    else:
        return ret_helper(app, "You are not permitted to access this path: " + path, 403, "text/plain")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
