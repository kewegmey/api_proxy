# API_Proxy
A simple (maybe too much so) proxy written in python/flask.

## Design Goals
A proxy service for REST APIs that implements white/black listing on specific paths/payloads/etc

Things it should do:

- Read in json/yaml for config 
- Witelist or blacklist depending on configuration
- Allow for searching any part of request for keywords to block or allow
- Easily integrate with API authentication to tie policy to users